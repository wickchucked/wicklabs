---
layout: page
title: About
permalink: /about/
weight: 2
---

<p class="intro">I'm a happy husband and proud father of three beautiful children.  When I'm not working full time at my current job, I build web applications on the side for those looking to make their ideas come to life.  I believe that your application should create a wonderful user experience from start to finish, built with a functional design, and a rock solid codebase.</p>
