'use strict';
// generated on 2015-02-19 using generator-gulp-webapp 0.1.0

var gulp  = require('gulp');


// load plugins
var $ = require('gulp-load-plugins')();

// gulp.task('styles', function () {
//     return gulp.src('app/styles/main.scss')
//         .pipe($.rubySass({
//             style: 'expanded',
//             precision: 10
//         }))
//         .pipe($.autoprefixer('last 1 version'))
//         .pipe(gulp.dest('.tmp/styles'))
//         .pipe($.size());
// });

// gulp.task('scripts', function () {
//     return gulp.src('app/scripts/**/*.js')
//         .pipe($.jshint())
//         .pipe($.jshint.reporter(require('jshint-stylish')))
//         .pipe($.size());
// });

// gulp.task('html', ['styles', 'scripts'], function () {
//     var jsFilter = $.filter('**/*.js');
//     var cssFilter = $.filter('**/*.css');

//     return gulp.src('app/*.html')
//         .pipe($.useref.assets({searchPath: '{.tmp,app}'}))
//         .pipe(jsFilter)
//         .pipe($.uglify())
//         .pipe(jsFilter.restore())
//         .pipe(cssFilter)
//         .pipe($.csso())
//         .pipe(cssFilter.restore())
//         .pipe($.useref.restore())
//         .pipe($.useref())
//         .pipe(gulp.dest('dist'))
//         .pipe($.size());
// });

// gulp.task('images', function () {
//     return gulp.src('app/images/**/*')
//         .pipe($.imagemin({
//             optimizationLevel: 3,
//             progressive: true,
//             interlaced: true
//         }))
//         .pipe(gulp.dest('dist/images'))
//         .pipe($.size());
// });

// gulp.task('fonts', function () {
//     return $.bowerFiles()
//         .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
//         .pipe($.flatten())
//         .pipe(gulp.dest('dist/fonts'))
//         .pipe($.size());
// });

// gulp.task('extras', function () {
//     return gulp.src(['app/*.*', '!app/*.html'], { dot: true })
//         .pipe(gulp.dest('dist'));
// });

// gulp.task('clean', function () {
//     return gulp.src(['.tmp', 'dist'], { read: false }).pipe($.clean());
// });

// gulp.task('build', ['html', 'images', 'fonts', 'extras']);

gulp.task('default', ['watch'], function () {
    // gulp.start('build');
});

gulp.task('connect', function () {
    var connect = require('connect');
    var app = connect()
        .use(require('connect-livereload')({ port: 35729 }))
        .use(connect.static('_site'))
        .use(connect.directory('_site'));

    require('http').createServer(app)
        .listen(9000)
        .on('listening', function () {
            console.log('Started connect web server on http://localhost:9000');
        });
});

gulp.task('serve', ['connect'], function () {
    require('opn')('http://localhost:9000');
});

gulp.task('watch', ['connect', 'serve'], function () {
    var server = $.livereload();

    // watch for changes

    gulp.watch([
        '_site/*.html',
        '_site/css/*.css'
    ]).on('change', function (file) {
        server.changed(file.path);
    });

    gulp.watch('app/styles/**/*.scss', ['styles']);
    gulp.watch('app/scripts/**/*.js', ['scripts']);
    gulp.watch('app/images/**/*', ['images']);
    gulp.watch('bower.json', ['wiredep']);
});
