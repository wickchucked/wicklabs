---
layout: page
title: Contact
permalink: /contact/
weight: 4
---

<h3>Get in touch with me to discuss your next project.</h3>

<p>Need advice?  Not sure which direction to go?  I can help, I have years of web development experience and I'm confident that I can provide a solution that will meet your needs.  Every project is different and sometimes it makes sense to build a custom application, other times it makes sense to use existing tools to get the job done.  Let's discuss your project to see which direction is best.</p>

<!--MAP-->
<div class="map">
    <iframe width="100%" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/?ie=UTF8&amp;ll=44.452832,-95.793015&amp;spn=0.054651,0.110378&amp;t=m&amp;z=13&amp;output=embed"></iframe>
</div>
<!--MAP-->
<!-- <h4 class="content-title">Let's Keep in Touch </h4> -->


<form action="//formspree.io/info@wicklabs.com" method="POST">
    <p>
    <input required="" type="text" name="name" id="name" />
    <label for="name">Your Name: <span class="required">(required)</span></label>
    </p>
    <p>
    <input required="" type="email" name="_replyto" id="email" />
    <label for="email">E-mail: <span class="required">(required)</span></label>
    </p>
    <p class="message-form-message">
      <textarea rows="5" cols="80" name="message" id="message"></textarea>
    </p>
    <input type="hidden" name="_next" value="thanks" />
    <input type="hidden" name="_subject" value="WickLabs contact submission!" />
    <input type="text" name="_gotcha" style="display:none" />
    <input type="submit" value="Send">
</form>