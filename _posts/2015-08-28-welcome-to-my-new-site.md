---
layout: post
title:  "Welcome to my new site!"
date:   2015-08-28 10:00:00
categories: website
---

Welcome to my first post on my new site!  I've recreated my site using the awesome static site builder Jekyll.
I'm hoping that this gives me more flexibility and allows me to start blogging and putting random tidbits out there.