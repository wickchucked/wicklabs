---
layout: post
title:  "BIS-Mapper syntax highlighting"
date:   2015-10-21 10:00:00
categories: website
---

So now that I have this new site created I guess it's time to put some content out there :smiley:.


A few years back I made a syntax highlighting file for Unisys BIS/MAPPER.  Here is a brief example of what the output looks like.

```bis
@     call,DEFINE_ function_ (<param>,<param>,<param>,<staus>h1) .
@     srh,-0 'd' 'field1','field2','field3' ¬,<var1>,<var2>,<var3> .
@     tot,-0 '' 'field1' ¬,+ .
@     lzr,-0 ,,<hln>i3 .
@0005:inc <hln>  rdl,-0,<hln>,0010 'field1','field2','field3' <field1>s,<field2>s,<field3>s .
@     gto 0005 .
@0010:rnm -2 .
```

If you want to add this to your site you can do so by adding the following files to your site.

```html
<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>BIS-Mapper syntax highlighting</title>

  <!-- Add these files -->
  <link rel="stylesheet" href="/css/railscasts.css">
  <script src="/scripts/hl.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>
  <!-- Add these files -->

</head>
```

To see what the page looked like before it got converted to html you can see the [raw source of this page](https://gitlab.com/wickchucked/wicklabs/raw/master/_posts/2015-10-21-bis-mapper-syntax-highlighting.md).


### Another example

BIS code calling BIS JavaScript

```bis
@.
@.    # Validation - JavaScript Regex Validation of Email Address
@     ldv <valid>h5=true,<field>s5=email .
@     call,"REGEXDEMO" validate(<field>,<email>,<valid>)  .
@     if <valid> eq false,(0182) .
@.
@.    # Validation - JavaScript Regex Validation of Phone Number
@     ldv <valid>h5=true,<field>s12='phone number' .
@     call,"REGEXDEMO" validate(<field>,<phone>,<valid>)  .
@     if <valid> eq false,(0182) .
@.
@.    # Validation - JavaScript Regex Validation of Zipcode
@     ldv <valid>h5=true,<field>s7=zipcode .
@     call,"REGEXDEMO" validate(<field>,<zip>,<valid>)  .
@     if <valid> eq false,(0182) .
@.
```

And the corresponding BIS JavaScript that's being called.

```js
/**
*
*  Register this Script as REGEXDEMO
*  Server Side JavaScript Validation with REGEX
*
**/

function validate(field,value,&valid) {

  var emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
  var phoneRegex = /^[\d]{3}-[\d]{3}-[\d]{4}$/;
  var zipRegex   = /^[\d]{5}(-[\d]{4})?$/;

  if (field === "email") {           // If field equals email contine into this block
    if(!value.match(emailRegex)) {   // If the value of email doesn''t match the regex
      valid = false;                 // Email is invalid
    }                                // Jumps to here if it is a good email
  }                                  // Else jump here if not email

  if (field === "phone number") {
    if(!value.match(phoneRegex)) {
      valid = false;
    }
  }

  if (field === "zipcode") {
    if(!value.match(zipRegex)) {
      valid = false;
    }
  }

}
```

### Web Controller

An example of an attempt at a controller in ICE.  Some common ICE run design patterns used.


```bis
@     ldv <run>a1=Y .
@     rer,0,e,2 0001 .
@     rnm,ecab$,g,erpt$ -8 .
@     gto lin+3 .
@0001:() .
@     rnm -8 ldv <run>h1=n if ENV_ eq 'PROD' . ; rep,-8,ecab$,g,erpt$ .
@     rsr,-8 001 .
@.
@.    # Initialize the application variables
@     rsr,APPCONFIG_ 0001 .
@.
@.    # Call the model
@.    call,MODEL_ 0001 () .
@.
@.    # Defined Actions
@     if <action> eq 'home        ',(0010) . ; .
@     if <action> eq 'contact     ',(0020) . ; .
@     if <action> eq 'about       ',(0030) . ; .
@     if <action> eq 'help        ',(0040) . ; .
@.
@.    # Action :: home
@0010:.
@     ldv <title>s20='Home' .
@     gto 0199 .
@.
@.    # Action :: contact
@0020:.
@     ldv <title>s20='Contact' .
@     gto 0199 .
@.
@.    # Action :: about
@0030:.
@     ldv <title>s20='About' .
@     gto 0199 .
@.
@.    # Action :: help
@0040:.
@     ldv <title>s20='Help' .
@     gto 0199 .
@.
@.    # Call the application view including headers and footers
@0199:rsr,APPLAYOUT_ 0001 .
```


### Example of the main view for the application

This is just a prototype that I've used on a few projects.

```bis
.Object Name: APPLAYOUT_
*    Version: 0001
*
*===============================================================================
:INCLUDE,ECAB$,B,0002 .
@001:() .
@     csr . Clear the calling subroutine - Will Return to ICESVCHND from here
@.
@. ---- Find the location of the view ----
@     rsr,APPCONFIG_ 0002 .
@.
@     brk,0,f,,y .
<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <title> <%<pagetitle>(p)%> </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <div class="container">

@.    # Render the header
@     rsr,HEADER_ .

    <section class="inner-container">
@     if <flash> eq 'true'  call,FLASH_ 0001 () .
@.    # Render the page template/html
@     rsr,TEMPLATE_ 0001 () .
    </section>

@.    # render the footer ----
@     rsr,FOOTER_ .

@     if ENV_ eq 'DEV' . ; gto lin+6
    <pre>
    ---- Debug info ----
    service: <service>
    action: <action>
    <pre>

    </div><!-- End of container div -->
  </body>
</html>
@     brk . End HTML Output Area
@     if <run> eq y dsx,-0 . ; .                             End HTML Output Area
@     return . Return to ICE Service Handler
```